import time

from optimum.neuron import NeuronStableDiffusionPipeline

model_id = "./stable-diffusion-v1-5-neuron"
input_shapes = {"batch_size": 1, "height": 512, "width": 512}

stable_diffusion = NeuronStableDiffusionPipeline.from_pretrained(
    model_id, export=False, **input_shapes, device_ids=[0, 1]
)

prompt = "a photo of an astronaut riding a horse on mars"

# Warmup
images = stable_diffusion(prompt)

num_iterations = 10

tick = time.time()
for _ in range(0, num_iterations):
    images = stable_diffusion(prompt)
tock = time.time()

print(f"Average time: {round((tock-tick)/num_iterations, 2)} seconds")
