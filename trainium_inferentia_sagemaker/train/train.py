import argparse
import os
from distutils.dir_util import copy_tree

import torch
import torch_xla.core.xla_model as xm
import torch_xla.distributed.parallel_loader as pl
import torch_xla.distributed.xla_backend
from datasets import load_from_disk
from torch.optim import AdamW
from torch.utils.data import DataLoader
from torch.utils.data.distributed import DistributedSampler
from transformers import AutoModelForSequenceClassification, AutoTokenizer

os.environ["TOKENIZERS_PARALLELISM"] = "false"

if __name__ == "__main__":

    parser = argparse.ArgumentParser()

    # hyperparameters sent by the client are passed as command-line arguments to the script.
    parser.add_argument("--model-name", type=str)
    parser.add_argument("--num-labels", type=int)
    parser.add_argument("--num-epochs", type=int, default=3)
    parser.add_argument("--learning-rate", type=float, default=1e-5)
    parser.add_argument("--train-batch-size", type=int, default=8)
    parser.add_argument("--num-samples", type=int)
    parser.add_argument("--seed", type=int, default=42)

    # Directories
    parser.add_argument("--dataset-dir", type=str, default=os.environ["SM_CHANNEL_TRAIN"])
    parser.add_argument("--cache-dir", type=str, default=os.environ["SM_CHANNEL_CACHE"])
    parser.add_argument("--model-dir", type=str, default=os.environ["SM_MODEL_DIR"])

    args, _ = parser.parse_known_args()

    print(f"Received args: {args}")

    torch.manual_seed(args.seed)

    device = "xla"

    # Populate Neuron cache
    copy_tree(f"{args.cache_dir}/neuron-compile-cache", "/var/tmp/neuron-compile-cache")

    # Initialize distributed training
    torch.distributed.init_process_group(device)
    world_size = xm.xrt_world_size()

    # Load and tokenize dataset
    def tokenize_function(examples):
        return tokenizer(examples["text"], padding="max_length", truncation=True)

    dataset = load_from_disk(args.dataset_dir)
    tokenizer = AutoTokenizer.from_pretrained(args.model_name)
    tokenized_dataset = dataset.map(tokenize_function, batched=True)
    tokenized_dataset = tokenized_dataset.remove_columns(["text"])
    tokenized_dataset = tokenized_dataset.rename_column("label", "labels")
    tokenized_dataset.set_format("torch")
    train_dataset = tokenized_dataset["train"]
    train_dataset = train_dataset.shuffle(seed=args.seed).select(range(args.num_samples))

    # Set up distributed data loader
    train_sampler = None
    if world_size > 1:
        train_sampler = DistributedSampler(
            train_dataset,
            num_replicas=world_size,
            rank=xm.get_ordinal(),
            shuffle=True,
        )
    train_loader = DataLoader(
        train_dataset,
        batch_size=args.train_batch_size,
        sampler=train_sampler,
        shuffle=not train_sampler,
    )

    train_device_loader = pl.MpDeviceLoader(train_loader, device)
    num_training_steps = args.num_epochs * len(train_device_loader)

    model = AutoModelForSequenceClassification.from_pretrained(
        args.model_name, num_labels=args.num_labels
    )
    model.to(device)

    optimizer = AdamW(model.parameters(), lr=args.learning_rate)

    model.train()
    for epoch in range(args.num_epochs):
        for batch in train_device_loader:
            batch = {k: v.to(device) for k, v in batch.items()}
            outputs = model(**batch)
            optimizer.zero_grad()
            loss = outputs.loss
            loss.backward()
            xm.optimizer_step(optimizer)
        print(
            "Epoch {}, rank {}, Loss {:0.4f}".format(
                epoch, xm.get_ordinal(), loss.detach().to("cpu")
            )
        )

    # Save compiled model, tokenizer and config
    checkpoint = {"state_dict": model.state_dict()}
    xm.save(checkpoint, f"{args.model_dir}/checkpoint.pt")
    tokenizer.save_pretrained(args.model_dir)
    model.config.save_pretrained(args.model_dir)

    # Save model cache
    copy_tree("/var/tmp/neuron-compile-cache", f"{args.model_dir}/neuron-compile-cache")
